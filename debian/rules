#!/usr/bin/make -f

export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export QT_SELECT=qt5

%:
	dh $@

override_dh_auto_configure:
	cp $(CURDIR)/debian/gcconfig.pri $(CURDIR)/src/
	cp $(CURDIR)/debian/qwtconfig.pri $(CURDIR)/qwt/
	dh_auto_configure -- -r QMAKE_DEFAULT_INCDIRS=
	# Switch static libraries to multiarch paths where relevant (#728351):
	cp src/src.pro src/src.pro.orig
	for staticlib in $$(grep -o '/lib/[^/]\+\.a' src/src.pro); do \
		libname=$$(echo $$staticlib|sed 's,.*/,,'); \
		if [ ! -e /usr/lib/$$libname ]; then \
			if [ -e /usr/lib/$(DEB_HOST_MULTIARCH)/$$libname ]; then \
				echo "Switching to multiarch path for library: $$libname"; \
				sed -i "s,$$staticlib,/lib/$(DEB_HOST_MULTIARCH)/$$libname," src/src.pro; \
			fi; \
		fi; \
	done

override_dh_auto_install:

execute_after_dh_installdocs:
	mkdir -p $(CURDIR)/debian/goldencheetah/usr/share/doc/goldencheetah/html
	cd doc/user && make clean && make && cp *.pdf $(CURDIR)/debian/goldencheetah/usr/share/doc/goldencheetah
	cd doc/web && make && cp *.html *.png *.jpg *.gp *.zones $(CURDIR)/debian/goldencheetah/usr/share/doc/goldencheetah/html

execute_before_dh_auto_clean:
	rm -rf qwt/lib qwt/qwtconfig.pri src/gcconfig.pri
	$(RM) -r qwt/src/obj/ src/*.o
	$(RM) qwt/src/moc/moc_*.cpp src/moc_*.cpp
	$(RM) doc/user/GC3-*.pdf doc/user/GC3-*.info doc/web/*.html
	-mv src/src.pro.orig src/src.pro
